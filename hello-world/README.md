# tutorial

https://kotlinlang.org/docs/tutorials/command-line.html

## 0. Install

```bash
brew install kotlin
```

## 1. Compile

```bash
kotlinc hello.kt -include-runtime -d hello.jar
```

## 2. Run

```bash
java -jar hello.jar
```
